<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL('/public/css/bootstrap.min.css') }}" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL('/public/css/main.css?v=3') }}" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <title>Trimitrasis</title>
  </head>
  <body>
    <!-- main section -->
    <header>
        <nav id="main-nav" class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="#">
                <img src="{{ URL('/public/images/main-logo.png') }}" alt="logo" title="logo">
            </a>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">About Us <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Our Solutions
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Solution by Product</a>
                        <a class="dropdown-item" href="#">Solution by Industry</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Our Solutions
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Solution by Product</a>
                        <a class="dropdown-item" href="#">Solution by Industry</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Carrier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Blog &amp; Events</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <section id="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-8 col-10">
                @for($i=0;$i<5;$i++)
                <div class="slide-item{{ $i == 0 ? ' active' : '' }}">
                    <div class="large-text">Slide {{ ($i + 1) }} Take your business to the next level through digital transformation</div>
                    <div class="small-text text-secondary">Small {{ ($i + 1) }}Quality deliver excellence</div>
                </div>
                @endfor
                </div>
                <div class="col-12">
                    <a class="btn btn-theme btn-primary" href="#">Get Started</a>
                    <ul class="slide-button" id="slide-button">
                    @for($i=0;$i<5;$i++)
                        <li data-slide="{{ $i }}" {{ $i == 0 ? 'class=active' : '' }}>&bull;</li>
                    @endfor
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- main section -->
    <!-- brand pattern -->
    <section class="brand-pattern">
        <div class="pattern-scroll">
            <div class="pattern-container">
                @for($i=0;$i<100;$i++)
                <img src="{{ URL('/public/images/pattern-'.(($i % 2) + 1).'.png') }}" alt="pattern" title=" pattern" />
                @endfor
            </div>
        </div>
    </section>
    <!-- end brand pattern -->
    <!-- we address challenge -->
    <section id="address-challenge">
        <div class="container">
            <h2 class="section-title">We Address Challenge</h2>
            <ul class="address-challenge-item">
                @foreach(CONFIG('data.address-challenge') as $key => $value)
                <li>
                    <div class="wrapper">
                        <div class="thumbnail"><img src="{{ URL($value['thumbnail']) }}" title="{{ $value['name'] }}" alt="{{ $value['name'] }}" /></div>
                        <div class="caption">{{ $value['name'] }}</div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </section>
    <!-- end we address challenge -->

    <!-- our solution -->
    <section class="solution">
        <div class="container">
            <h2 class="section-title">Our Solutions</h2>
            <h3 class="sub-section-title">Solution by Product</h3>
            <div class="row bottom-md">
                @foreach(CONFIG('data.solution-product') as $key => $value)
                <div class="col-lg-3 col-md-6 col-6 bottom-md">
                    <div class="item">
                        <div class="image"><img src="{{ URL($value['thumbnail']) }}" alt="services" title="services" /></div>
                        <h3 class="item-title">{{ $value['name'] }}</h3>
                        <div class="caption">{{ $value['description'] }}</div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-center bottom-md"><a href="#" class="btn btn-primary btn-theme">Talk to a consultant</a></div>
            <hr class="bottom-md" />
            <h3 class="sub-section-title">Solution by Industry</h3>
            <ul class="item-5">
                @foreach(CONFIG('data.solution-industry') as $key => $value)
                <li>
                    <div class="card">
                        <img src="{{ URL($value['thumbnail']) }}" class="card-img-top" alt="solution" title="solution">
                        <div class="card-body">
                            <h3 class="card-title">{{ $value['name'] }}</h3>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="text-center" style="margin: 40px 0;"><a href="#" class="btn btn-primary btn-theme">Talk to a consultant</a></div>
        </div>
    </section>
    <!-- end our solution -->

    <!-- about -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="about-video">
                        <img src="{{ URL('/public/images/about-video-thumbnail.png') }}" alt="about" title="about" style="width: 100%;" />
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <h2><img src="{{ URL('/public/images/about-title-rec.png') }}" alt="about" title="about" />&nbsp;&nbsp;&nbsp;About <span class="bold">Trimitrais</span></h2>
                    <p class="caption">Occaecat deserunt occaecat ipsum dolore voluptate dolor amet sit. Nulla esse voluptate officia exercitation duis quis nisi in eu esse irure deserunt aliqua. Ad nisi esse aliqua quis eiusmod est adipisicing quis aliqua deserunt. </p>
                    <h3 class="section-subtitle">Our Core Value</h3>
                    <div class="row">
                        @foreach(CONFIG('data.core-value') as $key => $value)
                        <div class="col-6">
                            <div class="core-value-item">
                                <img src="{{ URL($value['thumbnail']) }}" alt="about" title="about" />&nbsp;&nbsp;&nbsp;{{ $value['name'] }}
                            </div>
                        </div>
                        @endforeach
                        <div class="col-6">
                            <div style="margin: 20px 0;">
                                <a href="#" class="btn btn-primary btn-theme">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about -->

    <!-- quote -->
    <section class="quote">
        <div class="container">
            <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12">
                <div class="quote-body bottom-md">"Business is a game, played for fantastic stakes, and you’re in competition with experts. If you want to win, you have to learn to be a master of the game."</div>
                <div class="author">-Sidney Sheldon, Master of the Game</div>
            </div>
        </div>
    </section>
    <!-- end quote -->

    <!-- meet our awesome team -->
    <section id="team" style="background: url('{{ URL('/public/images/team-border-top.png') }}') top left 100% no-repeat; background-size: 100% auto;">
        <h2 class="section-title text-primary">Meet Our Owesome Team</h2>
        <p class="subtitle">Velit ut laboris minim ad sint tempor pariatur quis. Velit ut laboris minim ad sint tempor pariatur quis.</p>
        <div style="background: url('{{ URL('/public/images/team-bottom-background.png') }}') left bottom no-repeat; padding-bottom: 410px;">
            <div class="container">
                <div class="row">
                    @foreach(CONFIG('data.awesome-team') as $key => $value)
                    <div class="col-lg-3 col-md-6 col-12 member">
                        <div class="thumbnail">
                            <img src="{{ URL('/public/images/team-member.png') }}" alt="team" title="team" />
                            <div class="overlay">
                                <a href="#">View Profile</a>
                            </div>
                        </div>
                        <h3 class="name">{{ $value['name'] }}</h3>
                        <p class="title">{{ $value['title'] }}</p>
                        <p class="caption">This is caption with 3 lines only. Ea do tempor incididunt ullamco.Sint minim amet sint voluptate sunt velit nostrud laborum nulla Lorem fugiat nulla deserunt anim.</p>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="text-center"><a href="#" class="btn btn-primary btn-theme">View More</a></div>
        </div>
    </section>
    <!-- end meet our awesome team -->

    <!-- deliver value -->
    <section id="deliver-value">
        <div class="container">
            <h2 class="section-title">How We Deliver Our Value</h2>
            <p class="subtitle">Velit ut laboris minim ad sint tempor pariatur quis. Velit ut laboris minim ad sint tempor pariatur quis.</p>
            <div>
                <hr />
            </div>
            <div class="animation">
                <img src="{{ URL('/public/images/how-we-deliver-animation.gif') }}" alt="How we deliver" title="How we deliver" />
            </div>
        </div>
    </section>
    <!-- end deliver value -->

    <!-- calculate tco -->
    <section id="calculate-tco">
        <div class="container">
            <div class="col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-12">
                <div class="outer-container">
                    <div class="inner-container">
                        <h2><strong>Calculate your TCO</strong></h2>
                        <p>Realize your business goals by simplifying and accelerating your digital transformation. Let us help you to calculate the Total Cost of Ownership (TCO) so you can determine the right solution for your company.</p>
                        <div class="text-center"><a href="#" class="btn btn-primary btn-theme">Calculate your TCO here</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end calculate tco -->
    <svg style="width: 100%; height: 20px;">
		<ellipse style="fill: #0070C2;" id="Ellipse_294" rx="50%" ry="5" cx="50%" cy="5">
		</ellipse>
	</svg>
    <!-- what client said -->
    <section id="client-said">
        <h2 class="section-title text-primary">What Client Say</h2>
        <p class="subtitle text-secondary">Velit ut laboris minim ad sint tempor pariatur quis. Velit ut laboris minim ad sint tempor pariatur quis.</p>
        <div class="container">
            <div class="slick-container">
                @foreach(CONFIG('data.client-said') as $key => $value)
                <div class="item">
                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-lg-7 col-md-12 col-12">
                                <div class="card-body">
                                    <div class="row no-gutter">
                                        <div class="col-md-3 col-3"><img src="{{ URL('/public/images/client-say-profile.png') }}" alt="profile" title="profile" /></div>
                                        <div class="col-md-9 col-9">
                                            <h3 class="name">{{ $value['name'] }}</h3>
                                            <p class="title">CEO {{ $value['company'] }}</p>
                                        </div>
                                    </div>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. Voluptate adipisicing esse adipisicing mollit cillum est elit sint minim eiusmod pariatur ipsum ea sint.</p>
                                </div>
                                <div class="card-footer">
                                    <div class="company-logo"><img src="{{ URL($value['logo']) }}" alt="{{ $value['company'] }}" title="{{ $value['company'] }}" /></div>
                                    <p><span class="text-primary">Category</span>&nbsp;&nbsp;&nbsp;<span class="text-secondary">Mining</span></p>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-4 d-sm-none d-none d-md-none d-lg-block company-hidden" style="background: url('{{ URL($value['background']) }}') center no-repeat; background-size: cover;">
                                <div class="company-overlay"></div>
                                <div class="company-description">
                                    <h3 class="company-name">{{ $value['company'] }}</h3>
                                    <p>Sint et mollit adipisicing cillum ea ad dolor occaecat ipsum culpa. Laborum enim nulla Lorem minim velit. Tempor mollit tempor adipisicing dolore ipsum tempor id velit aute eu incididunt incididunt. Est culpa voluptate excepteur exercitation est ullamco aliquip Lorem aute minim laboris quis. Anim ea ipsum Lorem et cupidatat do est veniam sit ullamco nulla id. Ea ad exercitation in laboris Lorem id ut ea tempor Lorem.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-center">
                <button class="next-testimonial"><i class="fas fa-less-than"></i></button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button class="prev-testimonial"><i class="fas fa-greater-than"></i></button>
            </div>
        </div>
    </section>
    <!-- end what client said -->

    <!-- section blog -->
    <section id="blog">
        <h2 class="section-title text-primary">Blog</h2>
        <p class="subtitle text-secondary">Velit ut laboris minim ad sint tempor pariatur quis. Velit ut laboris minim ad sint tempor pariatur quis.</p>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="card highlighted">
                        <div class="blog-img" style="background: url('{{ URL('/public/images/blog-background-1.png') }}')"></div>
                        <div class="card-body">
                            <p class="text-secondary">March 14th, 2019</p>
                            <h5 class="card-title">Nisi eiusmod mollit proident aliqua ex nisi ullamcoAd anim consequat reprehenderit et laboris.</h5>
                            <p class="card-text text-secondary">Some quick example text to build on the card title and make up the bulk of the card's content. Non labore ad qui adipisicing est incididunt. Ullamco exercitation in nulla tempor tempor aliqua nisi consectetur. Ut id occaecat aliquip excepteur commodo eiusmod dolor consectetur ex sit et. Dolore esse ullamco nisi sit voluptate tempor nulla ut mollit mollit proident quis quis.</p>
                            <a href="#" class="btn btn-primary btn-theme stretched-link">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="row">
                        <div class="col-12">
                            @for($i=0;$i<2;$i++)
                            <div class="card">
                                <div class="row no-gutters">
                                    <div class="col-md-6 col-8">
                                        <div class="card-body">
                                            <p class="text-secondary">March 14th, 2019</p>
                                            <h5 class="card-title">Nisi eiusmod mollit proident aliqua ex nisi ullamco</h5>
                                            <p class="card-text text-secondary">Some quick example text to build on the card title and make up the bulk of the card's content Ex commodo labore dolore dolore tempor minim cillum ex fugiat ea. Ipsum anim veniam laborum mollit nisi duis. Eu quis nisi nisi nulla magna..</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-4 side" style="background: url('{{ URL('/public/images/blog-background-2.png') }}')"></div>
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section blog -->

    <!-- get in touch -->
    <section id="in-touch">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <h2 class="section-title">Get in Touch</h2>
                    <p class="subtitle">Velit ut laboris minim ad sint tempor pariatur quis. Velit ut laboris minim ad sint tempor pariatur quis.</p>
                    <div class="form-contact bottom-md">
                        <form>
                            <div class="form-row">
                                <div class=" form-group col-6">
                                    <input type="text" class="form-control" placeholder="Your Name" />
                                </div>
                                <div class="form-group col-6">
                                    <input type="text" class="form-control" placeholder="Phone Number" />
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email Address" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Company Name" />
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-theme btn-block">Get a Free Quote!</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <img src="{{ URL('/public/images/in-touch-map.png') }}" alt="map" title="map" style="width: 100%;" />
                </div>
            </div>
        </div>
    </section>
    <!-- end get in touch -->

    <!-- chat/inquiries -->
    <ul class="fixed-inquiries">
        <li><a href="#"><i class="fas fa-phone"></i><span>Phone</span></a></li>
        <li><a href="#"><span>Email</span>&nbsp;<i class="fas fa-envelope"></i></a></li>
        <li><a href="#"><span>Inquiries</span>&nbsp;<i class="fas fa-comment"></i></a></li>
    </ul>
    <!-- end chat/inquiries -->

    <!-- footer -->
    <footer>
        <div class="container" style="background: url('{{ URL('/public/images/footer-background.png') }}') right bottom no-repeat; padding-bottom: 50px;">
            <div class="row bottom-md padding-bottom-md border-bottom border-light">
                <div class="col-md-6 col-12">
                    <h2>Have any question?</h2>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</p>
                </div>
                <div class="col-md-6 col-12 faq">
                    <br />
                    <a href="#" class="btn btn-primary btn-theme">Frequently Ask Question</a>
                    &nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;
                    <a href="#" class="btn btn-transparent btn-theme">Live Chat!</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-12 col-12 bottom-md">
                    <img src="{{ URL('/public/images/footer-logo.png') }}" alt="logo" title="logo" />
                </div>
                <div class="col-lg-2 col-md-3 col-6">
                    <h3>Company</h3>
                    <ul class="menu">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Who We Are</a></li>
                        <li><a href="#">Achievement</a></li>
                        <li><a href="#">Portfolio</a></li>
                        <li><a href="#">Career</a></li>
                        <li><a href="#">News &amp; Events</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-6 bottom-md">
                    <h3>Contact</h3>
                    <p>info@trimitrasis.co.id</p>
                    <p>(+6221) 300 333 68</p>
                    <h3>Location</h3>
                    <p>SOHO Podomoro City unit 3106 Jl. Letjen S. Parman Kav. 28 Jakarta Barat 11470</p>
                </div>
                <div class="col-lg-4 col-md-5 col-12">
                    <h3>Subscribe to Trimitrasis via Email</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</p>
                    <form class="row">
                        <div class="col-8">
                            <input type="email" class="form-control input-transparent" placeholder="Email Address">
                        </div>
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-theme btn-block">Subscribe</button>
                        </div>
                    </form>
                    <div class="social">
                        <h3>Follow Us :</h3>
                        <a href="#"><i class="fab fa-instagram"></i></a>&nbsp;&nbsp;&nbsp;
                        <a href="#"><i class="fab fa-youtube"></i></a>&nbsp;&nbsp;&nbsp;
                        <a href="#"><i class="fab fa-facebook"></i></a>
                    </div>
                </div>
            </div>
            <div class="row text-center partner-logo" style="padding: 40px 0;">
                <div class="col-lg-2 col-md-6 col-6 bottom-md">
                    <img src="{{ URL('/public/images/footer-partner-sap.png') }}" alt="logo" title="logo" />
                </div>
                <div class="col-lg-3 col-md-6 col-6 bottom-md">
                    <img src="{{ URL('/public/images/footer-partner-microsoft.png') }}" alt="logo" title="logo" />
                </div>
                <div class="col-lg-2 col-md-6 col-12">
                    <img src="{{ URL('/public/images/footer-partner-salesforce.png') }}" alt="logo" title="logo" />
                </div>
            </div>
            <div class="row" style="padding: 40px 0;">
                <div class="col-12">
                    Consulting Agency. Copyright © 2019 Trimitrasis. All Rights Reserved
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#slide-button > li').click(function() {
                var index = $('#slide-button > li').index(this);
                $('#slide-button > li').each(function(i, obj) {
                    $(obj).removeClass('active');
                });
                $($('#slide-button > li')[index]).addClass('active');
                $('.slide-item').each(function(i, obj) {
                    $(obj).removeClass('active');
                });
                $($('.slide-item')[index]).addClass('active');
            });
            $('.slick-container').slick({
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
    prevArrow: $('.prev-testimonial'),
    nextArrow: $('.next-testimonial'),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
        });
    </script>
  </body>
</html>